import unittest
from anagrams import reverse_words

positive_cases = [
    ("abcd efgh", "dcba hgfe"),
    ("a1bcd efg!h", "d1cba hgf!e"),
    ("12$3!1@24", "12$3!1@24"),
    ("", "")
]

negative_cases = [
    ("a1bcd efg!h", "dcb1a h!gfe"),
    ("12$3!1@24", "12$3!1a@24"),
    ("a1bcd efg!h", "a1bcd efg!h")

]

negative_cases_type = [
    (1, "1")
]


class TestAnnagramReverseWords(unittest.TestCase):

    def test_positive(self):
        for text, reversed_text in positive_cases:
            self.assertEqual(reverse_words(text), reversed_text)

    def test_negative(self):
        for text, reversed_text in negative_cases:
            self.assertNotEqual(reverse_words(text), reversed_text)

    def test_wrong_case(self):
        for text, reversed_text in negative_cases_type:
            self.assertNotEqual(reverse_words(text), reversed_text)


if __name__ == '__main__':
    unittest.main()
