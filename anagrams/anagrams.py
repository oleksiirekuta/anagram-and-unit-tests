def generate_anagram(word):
    letters = [c for c in word if c.isalpha()]
    for c in word:
        if c.isalpha():
            yield letters.pop()
        else:
            yield c


def reverse_words(sentence):
    try:
        words = sentence.split()
        reversed_words = []
        for word in words:
            reversed_word = "".join(generate_anagram(word))
            reversed_words.append(reversed_word)
        return " ".join(reversed_words)
    except ValueError as e:
        print(type(e), 'not supported.')
    except (TypeError, AttributeError):
        print(type(sentence), 'not supported.')
    except Exception as e:
        print(e)


if __name__ == '__main__':
    cases = [
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
        ("", "")
    ]

    for text, reversed_text in cases:
        assert reverse_words(text) == reversed_text
